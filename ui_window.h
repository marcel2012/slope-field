/********************************************************************************
** Form generated from reading UI file 'window.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_H
#define UI_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "opengl.h"

QT_BEGIN_NAMESPACE

class Ui_Window
{
public:
	QWidget *centralWidget;
	QVBoxLayout *verticalLayout_2;
	QLabel *label;
	QHBoxLayout *horizontalLayout;
	QVBoxLayout *verticalLayout;
	QLabel *label_2;
	QLineEdit *lineEdit;
	QLabel *label_3;
	QListView *listView;
	QVBoxLayout *verticalLayout_4;
	QLabel *label_7;
	QVBoxLayout *verticalLayout_3;
	QLabel *label_5;
	QSlider *horizontalSliderScaleX;
	QLabel *label_6;
	QSlider *horizontalSliderScaleY;
	QLabel *label_4;
	QSlider *horizontalSliderStep;
	openGL *openGLWidget;
	QStatusBar *statusBar;

	void setupUi(QMainWindow *Window)
	{
		if (Window->objectName().isEmpty())
			Window->setObjectName(QStringLiteral("Window"));
		Window->resize(800, 800);
		QPalette palette;
		QBrush brush(QColor(0, 0, 0, 255));
		brush.setStyle(Qt::SolidPattern);
		palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
		QBrush brush1(QColor(255, 255, 255, 255));
		brush1.setStyle(Qt::SolidPattern);
		palette.setBrush(QPalette::Active, QPalette::Button, brush1);
		palette.setBrush(QPalette::Active, QPalette::Midlight, brush1);
		palette.setBrush(QPalette::Active, QPalette::Dark, brush1);
		palette.setBrush(QPalette::Active, QPalette::Mid, brush1);
		palette.setBrush(QPalette::Active, QPalette::Text, brush);
		palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
		palette.setBrush(QPalette::Active, QPalette::Base, brush1);
		palette.setBrush(QPalette::Active, QPalette::Window, brush1);
		palette.setBrush(QPalette::Active, QPalette::Shadow, brush1);
		palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush1);
		palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
		palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
		palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush1);
		palette.setBrush(QPalette::Inactive, QPalette::Dark, brush1);
		palette.setBrush(QPalette::Inactive, QPalette::Mid, brush1);
		palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
		palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
		palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
		palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
		palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
		palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush1);
		palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
		palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
		palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush1);
		palette.setBrush(QPalette::Disabled, QPalette::Dark, brush1);
		palette.setBrush(QPalette::Disabled, QPalette::Mid, brush1);
		palette.setBrush(QPalette::Disabled, QPalette::Text, brush1);
		palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
		palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
		palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
		palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
		palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
		Window->setPalette(palette);
		Window->setAutoFillBackground(false);
		centralWidget = new QWidget(Window);
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		verticalLayout_2 = new QVBoxLayout(centralWidget);
		verticalLayout_2->setSpacing(6);
		verticalLayout_2->setContentsMargins(11, 11, 11, 11);
		verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
		label = new QLabel(centralWidget);
		label->setObjectName(QStringLiteral("label"));
		QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
		sizePolicy.setHorizontalStretch(0);
		sizePolicy.setVerticalStretch(0);
		sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
		label->setSizePolicy(sizePolicy);
		label->setMaximumSize(QSize(16777215, 20));

		verticalLayout_2->addWidget(label);

		horizontalLayout = new QHBoxLayout();
		horizontalLayout->setSpacing(6);
		horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		verticalLayout = new QVBoxLayout();
		verticalLayout->setSpacing(6);
		verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
		label_2 = new QLabel(centralWidget);
		label_2->setObjectName(QStringLiteral("label_2"));
		label_2->setMaximumSize(QSize(16777215, 20));

		verticalLayout->addWidget(label_2);

		lineEdit = new QLineEdit(centralWidget);
		lineEdit->setObjectName(QStringLiteral("lineEdit"));
		QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
		sizePolicy1.setHorizontalStretch(0);
		sizePolicy1.setVerticalStretch(0);
		sizePolicy1.setHeightForWidth(lineEdit->sizePolicy().hasHeightForWidth());
		lineEdit->setSizePolicy(sizePolicy1);

		verticalLayout->addWidget(lineEdit);

		label_3 = new QLabel(centralWidget);
		label_3->setObjectName(QStringLiteral("label_3"));
		label_3->setMaximumSize(QSize(16777215, 20));

		verticalLayout->addWidget(label_3);

		listView = new QListView(centralWidget);
		listView->setObjectName(QStringLiteral("listView"));
		QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Minimum);
		sizePolicy2.setHorizontalStretch(0);
		sizePolicy2.setVerticalStretch(0);
		sizePolicy2.setHeightForWidth(listView->sizePolicy().hasHeightForWidth());
		listView->setSizePolicy(sizePolicy2);
		listView->setMaximumSize(QSize(16777215, 100));
		listView->setEditTriggers(QAbstractItemView::NoEditTriggers);

		verticalLayout->addWidget(listView);


		horizontalLayout->addLayout(verticalLayout);

		verticalLayout_4 = new QVBoxLayout();
		verticalLayout_4->setSpacing(6);
		verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
		verticalLayout_4->setContentsMargins(10, 10, 10, 10);
		label_7 = new QLabel(centralWidget);
		label_7->setObjectName(QStringLiteral("label_7"));
		QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Fixed);
		sizePolicy3.setHorizontalStretch(0);
		sizePolicy3.setVerticalStretch(0);
		sizePolicy3.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
		label_7->setSizePolicy(sizePolicy3);

		verticalLayout_4->addWidget(label_7);


		horizontalLayout->addLayout(verticalLayout_4);

		verticalLayout_3 = new QVBoxLayout();
		verticalLayout_3->setSpacing(6);
		verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
		label_5 = new QLabel(centralWidget);
		label_5->setObjectName(QStringLiteral("label_5"));
		label_5->setMaximumSize(QSize(16777215, 20));

		verticalLayout_3->addWidget(label_5);

		horizontalSliderScaleX = new QSlider(centralWidget);
		horizontalSliderScaleX->setObjectName(QStringLiteral("horizontalSliderScaleX"));
		horizontalSliderScaleX->setMinimum(5);
		horizontalSliderScaleX->setMaximum(300);
		horizontalSliderScaleX->setSingleStep(1);
		horizontalSliderScaleX->setOrientation(Qt::Horizontal);

		verticalLayout_3->addWidget(horizontalSliderScaleX);

		label_6 = new QLabel(centralWidget);
		label_6->setObjectName(QStringLiteral("label_6"));
		label_6->setMaximumSize(QSize(16777215, 20));

		verticalLayout_3->addWidget(label_6);

		horizontalSliderScaleY = new QSlider(centralWidget);
		horizontalSliderScaleY->setObjectName(QStringLiteral("horizontalSliderScaleY"));
		horizontalSliderScaleY->setMinimum(5);
		horizontalSliderScaleY->setMaximum(300);
		horizontalSliderScaleY->setSingleStep(1);
		horizontalSliderScaleY->setOrientation(Qt::Horizontal);

		verticalLayout_3->addWidget(horizontalSliderScaleY);

		label_4 = new QLabel(centralWidget);
		label_4->setObjectName(QStringLiteral("label_4"));
		label_4->setMaximumSize(QSize(16777215, 20));

		verticalLayout_3->addWidget(label_4);

		horizontalSliderStep = new QSlider(centralWidget);
		horizontalSliderStep->setObjectName(QStringLiteral("horizontalSliderStep"));
		horizontalSliderStep->setMinimum(5);
		horizontalSliderStep->setMaximum(100);
		horizontalSliderStep->setSingleStep(1);
		horizontalSliderStep->setOrientation(Qt::Horizontal);

		verticalLayout_3->addWidget(horizontalSliderStep);


		horizontalLayout->addLayout(verticalLayout_3);


		verticalLayout_2->addLayout(horizontalLayout);

		openGLWidget = new openGL(centralWidget);
		openGLWidget->setObjectName(QStringLiteral("openGLWidget"));
		QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Preferred);
		sizePolicy4.setHorizontalStretch(0);
		sizePolicy4.setVerticalStretch(0);
		sizePolicy4.setHeightForWidth(openGLWidget->sizePolicy().hasHeightForWidth());
		openGLWidget->setSizePolicy(sizePolicy4);

		verticalLayout_2->addWidget(openGLWidget);

		Window->setCentralWidget(centralWidget);
		statusBar = new QStatusBar(Window);
		statusBar->setObjectName(QStringLiteral("statusBar"));
		Window->setStatusBar(statusBar);

		retranslateUi(Window);

		QMetaObject::connectSlotsByName(Window);
	} // setupUi

	void retranslateUi(QMainWindow *Window)
	{
		Window->setWindowTitle(QApplication::translate("Window", "Slope field - Marcel Korpal", Q_NULLPTR));
		label->setText(QApplication::translate("Window", "Kalkulator wymaga u\305\274ycia odwrotnej notacji polskiej. Operandami musz\304\205 by\304\207 cyfry", Q_NULLPTR));
		label_2->setText(QApplication::translate("Window", "Podaj wz\303\263r:", Q_NULLPTR));
		lineEdit->setPlaceholderText(QApplication::translate("Window", "2x*y/", Q_NULLPTR));
		label_3->setText(QApplication::translate("Window", "lub wybierz z listy:", Q_NULLPTR));
		label_7->setText(QApplication::translate("Window", "Dost\304\231pne funkcje dwuargumentowe:\n"
			"+ dodawanie\n"
			"- odejmowanie\n"
			"* mno\305\274enie\n"
			"/ dzielenie\n"
			"^ pot\304\231gowanie\n"
			"\n"
			"Funkcja jednoargumentowe:\n"
			"| (pionowa kreska) warto\305\233\304\207 bezwzgl\304\231dna\n"
			"s sinus\n"
			"c cosinus\n"
			"t tangens\n"
			"l (L) logarytm naturalny\n"
			"\n"
			"Zmienne:\n"
			"x, y\n"
			"\n"
			"Sta\305\202e:\n"
			"p liczba Pi\n"
			"e podstawa logarytmu naturalnego", Q_NULLPTR));
		label_5->setText(QApplication::translate("Window", "Skala X:", Q_NULLPTR));
		label_6->setText(QApplication::translate("Window", "Skala Y:", Q_NULLPTR));
		label_4->setText(QApplication::translate("Window", "Krok:", Q_NULLPTR));
	} // retranslateUi

};

namespace Ui {
	class Window : public Ui_Window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_H