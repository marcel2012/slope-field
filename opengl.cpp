#include "opengl.h"
openGL::openGL(QWidget *s) {
	background = QBrush(QColor(245, 245, 255));
}

void openGL::setEquation(const char s[]) {
	int i = 0;
	for (; s[i]; i++)
		equation[i] = s[i];
	equation[i] = 0;
	update();
}

void openGL::setStatusBar(QStatusBar *statusBar) {
	this->statusBar = statusBar;
}

void openGL::paintGL()
{
	glEnable(GL_MULTISAMPLE);
	glEnable(GL_LINE_SMOOTH);

	painter = new QPainter(this);

	painter->setRenderHint(QPainter::Antialiasing);
	painter->setRenderHint(QPainter::HighQualityAntialiasing);

	painter->fillRect(this->rect(), background);
	drawAxis();
	painter->setPen(QColor(0, 0, 255));
	drawChart();
	painter->end();
}

void openGL::drawAxis() {
	int width = this->width();
	int height = this->height();
	painter->drawLine(0, height / 2, width, height / 2);
	painter->drawLine(width / 2, 0, width / 2, height);
	char tmp[256];
	int x = width / 2, i = 0;
	for (; x >= 0; x -= scaleX, i--);
	int y0 = height / 2, j0 = 0;
	for (; y0 >= 0; y0 -= scaleY, j0++);
	for (; x < width; x += scaleX, i++) {
		if (i && i % 5 == 0) {
			sprintf(tmp, "%d", i);
			painter->drawText(x, height / 2 - LINE_LENGTH, QString::fromStdString(tmp));
		}
		for (int y = y0, j = j0; y < height; y += scaleY, j--) {
			if (!i && j && j % 5 == 0) {
				sprintf(tmp, "%d", j);
				painter->drawText(width / 2 + LINE_LENGTH, y, QString::fromStdString(tmp));
			}
			long double *a = calcA(equation, i, j);
			if (a != NULL) {
				long double dx = LINE_LENGTH / sqrt(1 + (*a)*(*a));
				long double dy = -dx * (*a);
				painter->drawLine(x - dx, y - dy, x + dx, y + dy);
			}
		}
	}
}

QPoint openGL::dataToPoint(std::pair<double, double> data) {
	return QPoint(width() / 2.0 + data.first*scaleX, height() / 2.0 - data.second*scaleY);
}

void openGL::drawChart() {
	long double maxX = width() / 2.0 / scaleX, maxY = height() / 2.0 / scaleY;
	std::vector<std::pair<double, double>> output1, output2;
	output2.push_back(std::make_pair(startX, startY));
	calculateEquation(equation, startX, startY, diff, output1, -maxX, maxX, -maxY, maxY);
	calculateEquation(equation, startX, startY, -diff, output2, -maxX, maxX, -maxY, maxY);

	QPainterPath path;
	path.moveTo(dataToPoint(output2[output2.size() - 1]));
	for (int i = output2.size() - 1; i >= 0; i--)
		path.lineTo(dataToPoint(output2[i]));
	for (int i = 0; i < output1.size(); i++)
		path.lineTo(dataToPoint(output1[i]));
	painter->drawPath(path);

	if (statusBar != NULL) {
		char tmp[256];
		sprintf(tmp, "Punktow: %d, krok %.8llf", output1.size() + output2.size(), diff);
		statusBar->clearMessage();
		statusBar->showMessage(QString::fromStdString(tmp));
	}
}

void openGL::setScaleX(int x) {
	this->scaleX = x;
	update();
}

void openGL::setScaleY(int y) {
	this->scaleY = y;
	update();
}

int openGL::getScaleX() {
	return scaleX;
}

int openGL::getScaleY() {
	return scaleY;
}

void openGL::setDiff(int diff) {
	this->diff = diff * diff / 10000.0;
	update();
}

int openGL::getDiff() {
	long double v = diff * 10000.0;
	return sqrt(v);
}

void openGL::mouseReleaseEvent(QMouseEvent *event) {
	startX = (event->pos().x() - width() / 2.0) / scaleX;
	startY = (height() / 2.0 - event->pos().y()) / scaleY;
	update();
}