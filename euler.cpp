#include "euler.h"

long double *charToValue(char c, long double x, long double y) {
	long double *out=new long double;
	if (c == 'x')
		*out=x;
	else if (c == 'y')
		*out=y;
	else if (c == 'p')
		*out=M_PI;
	else if (c == 'e')
		*out=M_E;
	else if (c >= '0' && c <= '9')
		*out=c - '0';
	else
		out=NULL;
	return out;
}

long double *calcA(const char rpn[], const long double &x, const long double &y) {
	std::stack<long double> list;
	long double a, b;
	for (int i = 0; rpn[i]; i++) {
		switch (rpn[i]) {
		case '+':
			if(list.size()>1){
				b = list.top();
				list.pop();
				a = list.top();
				list.pop();
				list.push(a + b);
			}
			else
				return NULL;
			break;
		case '-':
			if(list.size()>1){
				b = list.top();
				list.pop();
				a = list.top();
				list.pop();
				list.push(a - b);
			}
			else
				return NULL;
			break;
		case '*':
			if(list.size()>1){
				b = list.top();
				list.pop();
				a = list.top();
				list.pop();
				list.push(a*b);
			}
			else
				return NULL;
			break;
		case '/':
			if(list.size()>1){
				b = list.top();
				list.pop();
				a = list.top();
				list.pop();
				list.push(a / b);
			}
			else
				return NULL;
			break;
		case '^':
			if(list.size()>1){
				b = list.top();
				list.pop();
				a = list.top();
				list.pop();
				list.push(pow(a, b));
			}
			else
				return NULL;
			break;
		case 's':
			if(list.size()){
				a = list.top();
				list.pop();
				list.push(sin(a));
			}
			else
				return NULL;
			break;
		case 'c':
			if(list.size()){
				a = list.top();
				list.pop();
				list.push(cos(a));
			}
			else
				return NULL;
			break;
		case 't':
			if(list.size()){
				a = list.top();
				list.pop();
				list.push(tan(a));
			}
			else
				return NULL;
			break;
		case '|':
			if(list.size()){
				a = list.top();
				list.pop();
				list.push(abs(a));
			}
			else
				return NULL;
			break;
		case 'l':
			if(list.size()){
				a = list.top();
				list.pop();
				list.push(log(a));
			}
			else
				return NULL;
			break;
		default:
			long double *c = charToValue(rpn[i], x, y);
			if(c==NULL)
				return NULL;
			list.push(*c);
			break;
		}
	}
	if (list.size() != 1)
		return NULL;
	else {
		long double *c = new long double;
		*c = list.top();
		return c;
	}
}

void calculateEquation(const char rpn[], const long double &x, const long double &y, const long double &h, std::vector<std::pair<double, double>> &output, long double xMin, long double xMax, long double yMin, long double yMax) {
	if (x >= xMin && x <= xMax && y >= yMin && y <= yMax && output.size() <= MAX_POINTS) {
		long double *a = calcA(rpn, x, y);
		if (a == NULL)
			printf("error in rpn form\n");
		else {
			long double yp = y + *a*h, xp = x + h;
			output.push_back(std::make_pair(xp, yp));
			calculateEquation(rpn, xp, yp, h, output, xMin, xMax, yMin, yMax);
		}
	}
}