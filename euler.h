#ifndef EULER_H
#define EULER_H

#include <vector>
#include <stack>
#include <math.h>
#include <stdio.h>

#define MAX_POINTS 20000

long double *charToValue(char c, long double x, long double y);

void calculateEquation(const char rpn[], const long double &x, const long double &y, const long double &h, std::vector<std::pair<double, double>> &output, long double xMin, long double xMax, long double yMin, long double yMax);

long double *calcA(const char rpn[], const long double &x, const long double &y);

#endif // EULER_H