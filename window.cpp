#include "window.h"
#include "ui_window.h"

std::vector<std::string> defaultList;

Window::Window(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::Window)
{
	defaultList.push_back("ex^x+ex^/");
	defaultList.push_back("x2^y*");
	defaultList.push_back("xy+s");
	defaultList.push_back("xy-s");
	defaultList.push_back("xy2^-s|");
	defaultList.push_back("yx2^/1x/+");
	defaultList.push_back("ex2^y+^l");
	defaultList.push_back("x2^x-2-");

	ui->setupUi(this);
	ui->lineEdit->setText(defaultList[0].c_str());
	ui->openGLWidget->setStatusBar(ui->statusBar);
	ui->horizontalSliderScaleX->setValue(ui->openGLWidget->getScaleX());
	ui->horizontalSliderScaleY->setValue(ui->openGLWidget->getScaleY());
	ui->horizontalSliderStep->setValue(ui->openGLWidget->getDiff());

	QStringListModel *sModel = new QStringListModel();
	QStringList sList;
	for (int i = 0; i < defaultList.size(); i++)
		sList.push_back(defaultList[i].c_str());
	sModel->setStringList(sList);
	ui->listView->setModel(sModel);
}

Window::~Window()
{
	delete ui;
}

void Window::on_lineEdit_textChanged(const QString &str)
{
	ui->openGLWidget->setEquation(str.toStdString().c_str());
}

void Window::on_horizontalSliderScaleX_valueChanged(int value)
{
	ui->openGLWidget->setScaleX(value);
}

void Window::on_horizontalSliderScaleY_valueChanged(int value)
{
	ui->openGLWidget->setScaleY(value);
}

void Window::on_horizontalSliderStep_valueChanged(int value)
{
	ui->openGLWidget->setDiff(value);
}

void Window::on_listView_activated(const QModelIndex &index)
{
	ui->lineEdit->setText(defaultList[index.row()].c_str());
}