#ifndef OPENGL_H
#define OPENGL_H

#include <QPainter>
#include <QBrush>
#include <QColor>
#include <QPaintEvent>
#include <QWidget>
#include <QOpenGLWidget>
#include <QOpenGLContext>
#include <QStatusBar>
#include "euler.h"
#include <stdio.h>
#define LINE_LENGTH 5

class openGL : public QOpenGLWidget
{
	Q_OBJECT
public:
	openGL(QWidget *s);
	void setEquation(const char s[]);
	void setStatusBar(QStatusBar *statusBar);
	void setScaleX(int x);
	void setScaleY(int y);
	void setDiff(int diff);
	int getScaleX();
	int getScaleY();
	int getDiff();
protected:
	void paintGL();
	void mouseReleaseEvent(QMouseEvent *event) override;
private:
	char equation[255] = "";
	int scaleX = 30, scaleY = 30;
	long double startX = 2, startY = 2;
	long double diff = 5 / 1000.0;
	QBrush background;
	QPainter *painter = NULL;
	QStatusBar *statusBar = NULL;
	void drawAxis();
	void drawChart();
	QPoint dataToPoint(std::pair<double, double> data);
};

#endif // OPENGL_H