#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include <QStringListModel>
#include "opengl.h"

namespace Ui {
	class Window;
}

class Window : public QMainWindow
{
	Q_OBJECT

public:
	explicit Window(QWidget *parent = 0);
	~Window();

private slots:
	void on_lineEdit_textChanged(const QString &arg1);

	void on_horizontalSliderScaleX_valueChanged(int value);

	void on_horizontalSliderScaleY_valueChanged(int value);

	void on_horizontalSliderStep_valueChanged(int value);

	void on_listView_activated(const QModelIndex &index);

private:
	Ui::Window *ui;
};

#endif // WINDOW_H